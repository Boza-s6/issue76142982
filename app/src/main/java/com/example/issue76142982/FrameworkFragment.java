package com.example.issue76142982;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class FrameworkFragment extends Fragment {

    public static final StringBuilder sb = new StringBuilder();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_framework, container, false);

        TextView log = v.findViewById(R.id.logText);
        log.setText(sb.toString());
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        String msg = "Fragment#onDestroyView()";
        Log.w("Framework", msg);
        sb.append(msg).append('\n');
    }
}
