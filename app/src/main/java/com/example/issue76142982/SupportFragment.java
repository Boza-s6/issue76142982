package com.example.issue76142982;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class SupportFragment extends Fragment {
    public static final StringBuilder sb = new StringBuilder();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_support, container, false);
        TextView logText = v.findViewById(R.id.logTextSupport);
        logText.setText(sb.toString());
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        String msg = "Fragment#onDestroyView()";
        Log.w("Support", msg);
        sb.append(msg).append('\n');
    }
}
