package com.example.issue76142982;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class SupportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, new
                    SupportFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        String msg = "Activity#onDestroy()";
        Log.w("Support", msg);
        SupportFragment.sb.append(msg).append('\n');
        super.onDestroy();
    }
}
