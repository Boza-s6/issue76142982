package com.example.issue76142982;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class FrameworkActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().add(android.R.id.content, new
                    FrameworkFragment()).commit();
        }
    }

    @Override
    protected void onDestroy() {
        String msg = "Activity#onDestroy()";
        Log.w("Framework", msg);
        FrameworkFragment.sb.append(msg).append('\n');
        super.onDestroy();
    }
}
